# enter front-end directory and start npm
npm-start:
	cd front-end && npm start

# enter front-end directory, install and start npm
npm-install:
	cd front-end && npm install && npm start

# pull recent changes
pull:
	git pull
	git status
