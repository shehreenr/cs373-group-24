import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Image from 'react-bootstrap/Image'
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import {Link} from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import Highlighter from "react-highlight-words";
import './program_card.css'

async function getImage(countyName) {
  console.log(countyName)
  try {
    const response = await fetch('https://en.wikipedia.org/w/api.php?action=query&titles=' +String(countyName) +'_County,_Texas&prop=pageimages&format=json&pithumbsize=250&origin=*');
    const data = await response.json();
    const result = JSON.parse(JSON.stringify(data.query.pages));
    const filtered = Object.values(result)
    const finalResult = filtered.find(value => value.title.includes(countyName));
    console.log(finalResult.thumbnail.source);
    return finalResult.thumbnail.source;
  } catch (error) {
    return [];
  }
}

function ProgramCard(props) {
    const [imageUrl, setImage] = useState("");
    const navigate = useNavigate();
    const queries = props.cardQuery ? props.cardQuery.split(' ') : []
    
    const [isVisible, setIsVisible] = useState(false);
    const [animationPlayed, setAnimationPlayed] = useState(false);
    const cardRef = React.useRef();

    useEffect(() => {
        const handleScroll = () => {
            const elementTop = cardRef.current.getBoundingClientRect().top;
            const isVisible = elementTop < window.innerHeight - 100;
            setIsVisible(isVisible);
        };

        window.addEventListener('scroll', handleScroll);
        handleScroll();
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);
    
    useEffect(() => {
        if (isVisible && !animationPlayed) {
            setAnimationPlayed(true);
        }
    }, [isVisible, animationPlayed]);
    
    useEffect(() => {
      const fetchPost = async () => {
        var countyName = props.county;
        const response = await getImage(countyName);
        setImage(response);
      }
      fetchPost();
    }, [props.county]);

    return(
        <Card className={`program-card ${isVisible || animationPlayed ? 'visible' : ''}`} ref={cardRef}>
            <Card.Img as={Image} variant="top" width={150} height={175} src={imageUrl}/>
            <Card.Body>
                <Card.Header className="program-text-head"><Highlighter searchWords={queries} textToHighlight={props.name}/></Card.Header>
                <Card.Text className="program-text-start"><b>Counties Served:</b> <Highlighter searchWords={queries} textToHighlight={props.county}/></Card.Text>
                <Card.Text className="program-text-start"><b>Address:</b> <Highlighter searchWords={queries} textToHighlight={props.address}/></Card.Text>
                <Card.Text className="program-text-start"><b>Phone Number:</b> <Highlighter searchWords={queries} textToHighlight={props.number}/></Card.Text>
                <Card.Text className="program-text-start"><b>Spanish Resources:</b> <Highlighter searchWords={queries} textToHighlight={props.spanish ? "Spanish Resources Available" : "No Spanish Resources"}/></Card.Text>
                <Button className="program-learn-more" onClick={() => navigate(`/literacy_programs/${props.id}`, {state: {props: props}})}><b>Learn More</b></Button>
            </Card.Body>
        </Card>
    )
}

export default ProgramCard; 