export const program_information = [
  {
      imageSrc: "/assets/programs/region_one_logo.jpg",
      mapSrc: "https://www.openstreetmap.org/export/embed.html?bbox=-98.18365305662157%2C26.310213025316166%2C-98.18164944648743%2C26.31108099317783&amp;layer=mapnik",
      name: "Region 1 Education Service Center",
      county: "Hidalgo",
      library: ["Sergeant Fernando de la Rosa Memorial Library"],
      address: "1900 W Schunior St, Edinburg, TX 78541",
      number: "(956) 984-6270",
      spanish: "Spanish resources available",
      id: 0
  },
  {
    imageSrc: "/assets/programs/abeline-program.png",
    mapSrc: "https://www.openstreetmap.org/export/embed.html?bbox=-99.742169380188%2C32.4411545803524%2C-99.72614049911499%2C32.44769170982527&amp;layer=mapnik",
    name: "Workforce Solutions of West Central Texas",
    county: "Taylor",
    address: "500 Chestnut St #1100, Abilene, TX 79602",
    number: "(325) 795-4301",
    spanish: "Spanish resources available",
    id: 1
  },
  {
    imageSrc: "/assets/programs/abeline-adult-ed-logo.jpg",
    mapSrc: "https://www.openstreetmap.org/export/embed.html?bbox=-99.7483277320862%2C32.43409177473888%2C-99.73442316055299%2C32.4406294164136&amp;layer=mapnik",
    name: "Abilene ISD Adult Education Programs",
    county: "Shackelford",
    address: "1929 South 11th Street, Abilene, TX, 79602",
    number: "(325) 671-4419",
    spanish: "Spanish resources available",
    id: 2
  }
];