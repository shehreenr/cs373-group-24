import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

URL = 'https://www.literatetx.me/'

class TestSite(unittest.TestCase):
  def setUp(self) -> None:
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--incognito')
    options.add_argument('--no-sandbox')
    options.add_argument('--start-maximized')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--disable-popup-blocking')
    options.add_argument('--ignore-certificate-errors')
    self.driver = webdriver.Chrome(options=options)
    self.driver.implicitly_wait(10)
    self.driver.get(URL)
    
  def tearDown(self) -> None:
    self.driver.quit()
  
  def navigate_to_page(self, link_text):
    link = self.driver.find_element(By.LINK_TEXT, link_text)
    link.click()

  # Test 1: splash page correctly renders text
  def test_splash_text(self):
    self.driver.get(URL)
    actual = self.driver.find_element(By.CLASS_NAME, 'about-header').text
    expected = 'Texas has the third lowest adult literacy rate in the nation at 71.8%. Illiteracy leads to fewer employment opportunities, lower incomes, and a higher likelihood of committing crimes. It also hurts future generations, as parents who are illiterate or have a low level of literacy often raise children who also struggle with literacy and education as a whole. LiterateTX aims to raise awareness about the prevalence of TX adults who struggle with illiteracy, and provide illiterate and low-literacy adults with resources to improve their literacy skills.\nCounties: Browse counties located in Texas. Learn basic demographic information as well as literacy statistics regarding each county. We also provide data on the proportion and range of illiterate adults in the population of each county. Find libraries and literacy programs located in these counties.\nAdult Literacy Programs: Search programs and resources that are aimed at increasing literacy in Texan adults. Learn about the counties that each program is based in, its address, phone number, and whether or not they provide additional support for Spanish speakers. Find libraries located in the counties that these programs serve.\nLibraries: Find public libraries in Texas where illiterate and low-literacy adults can access reading material and improve their literacy. Learn which city and county each library is located, how large it is, as well as the number of audio and video items that are available for checkout. We also provide links to literacy programs and resources that can be found located within the same city/county as the library.'
    self.assertEqual(actual, expected)

  # Test 2: confirm group name on nav bar loads
  def test_nav_bar(self):
    self.driver.get(URL)
    nav_bar = self.driver.find_element(By.CLASS_NAME, 'navbar-brand')
    actual = nav_bar.text
    expected = "LiterateTX"
    self.assertEqual(actual, expected)

  # Test 3: confirm Home link on nav bar
  def test_nav_bar_home(self):
    self.driver.get(URL)
    nav_bar = self.driver.find_elements(By.CLASS_NAME, 'navbar-brand')
    actual = nav_bar[1].text
    expected = "Home"
    self.assertEqual(actual, expected)

    self.navigate_to_page("Home")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

  # Test 4: confirm About link on nav bar
  def test_nav_bar_about(self):
    self.driver.get(URL)
    nav_bar = self.driver.find_elements(By.CLASS_NAME, 'navbar-brand')
    actual = nav_bar[2].text
    expected = "About"
    self.assertEqual(actual, expected)

  # Test 5: confirm Counties link on nav bar
  def test_nav_bar_counties(self):
    self.driver.get(URL)
    nav_bar = self.driver.find_elements(By.CLASS_NAME, 'navbar-brand')
    actual = nav_bar[3].text
    expected = "Counties"
    self.assertEqual(actual, expected)
    
  # Test 6: confirm Literacy Programs link on nav bar
  def test_nav_bar_literacy_programs(self):
    self.driver.get(URL)
    nav_bar = self.driver.find_elements(By.CLASS_NAME, 'navbar-brand')
    actual = nav_bar[4].text
    expected = "Literacy Programs"
    self.assertEqual(actual, expected)
    
  # Test 7: confirm Literacy Programs link on nav bar
  def test_nav_bar_libraries(self):
    self.driver.get(URL)
    nav_bar = self.driver.find_elements(By.CLASS_NAME, 'navbar-brand')
    actual = nav_bar[5].text
    expected = "Libraries"
    self.assertEqual(actual, expected)

  # Test 8: test navigation to about page and back home
  def test_navigation_about(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page('About')
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/about_me')
    
    self.driver.back()
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

  # Test 9: test navigation to about page and back home
  def test_navigation_counties(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')
    
    self.navigate_to_page('Counties')
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/counties')

    self.driver.back()
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

  # Test 10: test navigation to about literacy programs page and back home
  def test_navigation_literacy_programs(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page('Literacy Programs')
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/literacy_programs')

    self.driver.back()
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

  # Test 11: test navigation to libraries page and back home
  def test_navigation_libraries(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page("Libraries")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/libraries')

    self.driver.back()
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

  # Test 12: test search all navigation from splash page
  def test_search_all_navigation(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    search_bar = self.driver.find_element(By.ID, "search-all")
    search_bar.send_keys("Shackelford")
    search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/search/Shackelford')

  # Test 13: test search all from counties
  def test_search_all_counties(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page("Counties")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/counties')

    search_bar = self.driver.find_element(By.ID, "search-all")
    search_bar.send_keys("Austin")
    search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/search/Austin')

  # Test 14: test search all from literacy programs
  def test_search_all_literacy_programs(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page("Literacy Programs")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/literacy_programs')

    search_bar = self.driver.find_element(By.ID, "search-all")
    search_bar.send_keys("Abilene")
    search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/search/Abilene')

  # Test 15: test search all from libraries
  def test_search_libraries(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page("Libraries")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/libraries')

    search_bar = self.driver.find_element(By.ID, "search-all")
    search_bar.send_keys("Advocacy")
    search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/search/Advocacy')

  # Test 16: test counties search
  def test_search_counties(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page("Counties")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/counties')

    search_bar = self.driver.find_element(By.ID, "search-counties")
    search_bar.send_keys("austin")
    search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/counties_search/austin')

  # Test 17: test literacy programs search
  def test_search_literacy_programs(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page("Literacy Programs")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/literacy_programs')

    search_bar = self.driver.find_element(By.ID, "search-programs")
    search_bar.send_keys("abilene")
    search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/programs_search/abilene')

  # Test 18: test libraries search
  def test_search_all_libraries(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page("Libraries")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/libraries')

    search_bar = self.driver.find_element(By.ID, "search-libraries")
    search_bar.send_keys("advocacy")
    search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/libraries_search/advocacy')

  # Test 19: test search all from counties search
  def test_multi_search_counties(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page("Counties")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/counties')

    county_search_bar = self.driver.find_element(By.ID, "search-counties")
    county_search_bar.send_keys("advocacy")
    county_search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/counties_search/advocacy')

    all_search_bar = self.driver.find_element(By.ID, "search-all")
    all_search_bar.send_keys("Advocacy")
    all_search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/search/Advocacy')
  
  # Test 20: test search all from literacy programs search
  def test_multi_search_programs(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page("Literacy Programs")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/literacy_programs')

    county_search_bar = self.driver.find_element(By.ID, "search-programs")
    county_search_bar.send_keys("alamo")
    county_search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/programs_search/alamo')

    all_search_bar = self.driver.find_element(By.ID, "search-all")
    all_search_bar.send_keys("Alamo")
    all_search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/search/Alamo')

  # Test 21: test search all from libraries search
  def test_multi_search_libraries(self):
    self.driver.get(URL)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/')

    self.navigate_to_page("Libraries")
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/libraries')

    library_search_bar = self.driver.find_element(By.ID, "search-libraries")
    library_search_bar.send_keys("amarillo")
    library_search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/libraries_search/amarillo')

    all_search_bar = self.driver.find_element(By.ID, "search-all")
    all_search_bar.send_keys("Amarillo ")
    all_search_bar.send_keys(Keys.RETURN)
    self.assertEqual(self.driver.current_url, 'https://www.literatetx.me/search/Amarillo')


if __name__ == '__main__':
  unittest.main()
