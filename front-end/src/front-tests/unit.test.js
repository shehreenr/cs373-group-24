import '@testing-library/jest-dom';
import { render, screen, cleanup, fireEvent, waitFor } from '@testing-library/react';
import React from "react";
import Navbar from '../components/navbar/navbar.js'
import ToolCard from "../components/about/tool_card.js";
import AboutCard from "../components/about/about_card.js";
import CountyCard from "../components/counties/county_card.js";
import Counties from "../components/counties/counties.js";
import ProgramCard from "../components/literacy_programs/program_card.js";
import LiteracyPrograms from "../components/literacy_programs/literacy_programs.js";
import LibraryCard from "../components/libraries/library_card.js";
import Libraries from "../components/libraries/libraries.js";
import { BrowserRouter as Router } from "react-router-dom";
afterEach(cleanup);

describe("Navbar Component", () => { 
    test("test 1: render navbar", () => {
        render(<Router><Navbar /></Router>);
        expect(screen.getByText('Home')).toBeInTheDocument();
        expect(screen.getByText('About')).toBeInTheDocument();
        expect(screen.getByText('Counties')).toBeInTheDocument();
        expect(screen.getByText('Literacy Programs')).toBeInTheDocument();
        expect(screen.getByText('Libraries')).toBeInTheDocument();
    });
});

describe("Navbar Search", () => { 
    test("test 2: render search bar", () => {
        const { getByTestId } = render(<Router><Navbar /></Router>);
        const searchBar = getByTestId("search-all");
        expect(searchBar).toBeInTheDocument();
    });
});

describe('Navbar Search', () => {
    it('test 3: search-all should render the input element', () => {
        const { getByPlaceholderText } = render(<Router><Navbar /></Router>);
        const inputElement = getByPlaceholderText('Search...');
        expect(inputElement).toBeInTheDocument();
    });
});

describe('Navbar Search', () => {
    it('test 4: search-all simulate inputing a query into search', async () => {
        const initialEntries = ['https://www.literatetx.me/'];
        const { getByTestId } = render(<Router><Navbar /></Router>);
        const searchInput = getByTestId('search-all');
        fireEvent.change(searchInput, { target: { value: 'example' } });
        expect(searchInput.value).toBe('example');
    });
});

describe("Counties Search", () => { 
    test("test 5: render counties search bar", () => {
        const { getByTestId } = render(<Router><Counties /></Router>);
        const searchBar = getByTestId("search-counties");
        expect(searchBar).toBeInTheDocument();
    });
});

describe('Counties Search', () => {
    it('test 6: search-counties should render the input element', () => {
        const { getByPlaceholderText } = render(<Router><Counties /></Router>);
        const inputElement = getByPlaceholderText('Search counties...');
        expect(inputElement).toBeInTheDocument();
    });
});

describe('Counties Search', () => {
    it('test 7: search-counties simulate inputing a query into search', async () => {
        const { getByTestId } = render(<Router><Counties /></Router>);
        const searchInput = getByTestId('search-counties');
        fireEvent.change(searchInput, { target: { value: 'example' } });
        expect(searchInput.value).toBe('example');
    });
});

describe("LiteracyProgram Search", () => { 
    test("test 8: render literacy program search bar", () => {
        const { getByTestId } = render(<Router><LiteracyPrograms /></Router>);
        const searchBar = getByTestId("search-programs");
        expect(searchBar).toBeInTheDocument();
    });
});

describe('LiteracyProgram Search', () => {
    it('test 9: search-programs should render the input element', () => {
        const { getByPlaceholderText } = render(<Router><LiteracyPrograms /></Router>);
        const inputElement = getByPlaceholderText('Search programs...');
        expect(inputElement).toBeInTheDocument();
    });
});

describe('LiteracyProgram Search', () => {
    it('test 10: search-programs simulate inputing a query into search', async () => {
        const { getByTestId } = render(<Router><LiteracyPrograms /></Router>);
        const searchInput = getByTestId('search-programs');
        fireEvent.change(searchInput, { target: { value: 'example' } });
        expect(searchInput.value).toBe('example');
    });
});

describe("Libraries Search", () => { 
    test("test 11: render literacy program search bar", () => {
        const { getByTestId } = render(<Router><Libraries /></Router>);
        const searchBar = getByTestId("search-libraries");
        expect(searchBar).toBeInTheDocument();
    });
});
describe('Libraries Search', () => {
    it('test 12: search-programs should render the input element', () => {
        const { getByPlaceholderText } = render(<Router><Libraries /></Router>);
        const inputElement = getByPlaceholderText('Search libraries...');
        expect(inputElement).toBeInTheDocument();
    });
});

describe('Libraries Search', () => {
    it('test 13: search-programs simulate inputing a query into search', async () => {
        const { getByTestId } = render(<Router><Libraries /></Router>);
        const searchInput = getByTestId('search-libraries');
        fireEvent.change(searchInput, { target: { value: 'example' } });
        expect(searchInput.value).toBe('example');
    });
});

describe('AboutCard Component', () => {
    it('test 14: about card render', () => {
        const sampleData = {
        imageSrc: "assets/people/jerry.png",
        name: "Jerry Lin",
        gitlab: "@jerry_lin",
        email: "jerry_lin@utexas.edu",
        role: "Front-end",
        bio: "I am a second year Computer Science major at UT Austin. I have interests in art, cooking, and video games."
        };

        render(<AboutCard {...sampleData}/>);

        const userElement = screen.getByText('@jerry_lin');
        expect(userElement).toBeInTheDocument();
    });
});

describe('AboutCard Component', () => {
    it('test 15: another about card render', () => {
        const sampleData = {
        imageSrc: 'assets/people/ritvik.png',
        name: "Ritvik Renikunta",
        gitlab: "@ritvik.renikunta",
        email: "ritvik.renikunta@utexas.edu",
        role: "Back-end",
        bio: "I am a third year CS student at UT Austin. Outside of academics, I enjoy working out and playing tennis."
        };

        render(<AboutCard {...sampleData}/>);

        const userElement = screen.getByText('@ritvik.renikunta');
        expect(userElement).toBeInTheDocument();
    });
});
  
describe('ToolCard', () => {
    it('Test 16: tool card render', () => {
        const tool = {
            imageSrc: "gitlab.png",
            name: "Gitlab"
        };

        render(<ToolCard {...tool} />);

        const toolName = screen.getByText('Gitlab');
        expect(toolName).toBeInTheDocument();
    });
});

describe('CountyCard', () => {
    it('test 17: county card render', () => {
        const county = {
            imageSrc: "/assets/counties/hidalgo.jpg",
            graphSrc: "/assets/counties/hidalgo-graph.png",
            embed: "https://data.census.gov/profile/Hidalgo_County,_Texas?g=050XX00US48215",
            name: "Hidalgo",
            population: "423295",
            num_illiterate: "211648",
            percent_illiterate: 50,
            upper_bound: "32.2",
            lower_bound: "68.5  ",
            county_link: "/counties",
            id: 1,
        };
        render(<Router><CountyCard {...county} /></Router>);

        const countyName = screen.getByText('Hidalgo');
        expect(countyName).toBeInTheDocument();

        const countyPopulation = screen.getByText('423295');
        expect(countyPopulation).toBeInTheDocument();
    });
});

describe('CountyCard', () => {
    it('test 18: another county card render', () => {
        const county = {
            imageSrc: "/assets/counties/taylor.jpg",
            graphSrc: "/assets/counties/taylor-graph.png",
            embed: "https://data.census.gov/profile/Taylor_County,_Texas?g=050XX00US48441",
            name: "Taylor",
            population: "89454",
            num_illiterate: "10734",
            percent_illiterate: 12,
            upper_bound: "5.9",
            lower_bound: "20.9",
            county_link: "/counties",
            id: 2
        };
        render(<Router><CountyCard {...county} /></Router>);

        const countyIlliterate = screen.getByText('10734');
        expect(countyIlliterate).toBeInTheDocument();

        const countyLower = screen.getByText('20.9');
        expect(countyLower).toBeInTheDocument();

        const countyUpper = screen.getByText('5.9');
        expect(countyUpper).toBeInTheDocument();
    });
});

describe('ProgramCard', () => {
    it('test 19: program card render', () => {
        const program = {
            imageSrc: "/assets/programs/region_one_logo.jpg",
            mapSrc: "https://www.openstreetmap.org/export/embed.html?bbox=-98.18365305662157%2C26.310213025316166%2C-98.18164944648743%2C26.31108099317783&amp;layer=mapnik",
            name: "Region 1 Education Service Center",
            county: "Hidalgo",
            library: ["Sergeant Fernando de la Rosa Memorial Library"],
            address: "1900 W Schunior St, Edinburg, TX 78541",
            number: "(956) 984-6270",
            spanish: "Spanish resources available",
            id: 0
        };
        render(<Router><ProgramCard {...program} /></Router>);

        const programCounty = screen.getByText('Hidalgo');
        expect(programCounty).toBeInTheDocument();

        const programAddress = screen.getByText('1900 W Schunior St, Edinburg, TX 78541');
        expect(programAddress).toBeInTheDocument();
    });
});

describe('ProgramCard', () => {
    it('test 20: another program card render', () => {
        const program = {
            imageSrc: "/assets/programs/abeline-program.png",
            mapSrc: "https://www.openstreetmap.org/export/embed.html?bbox=-99.742169380188%2C32.4411545803524%2C-99.72614049911499%2C32.44769170982527&amp;layer=mapnik",
            name: "Workforce Solutions of West Central Texas",
            county: "Taylor",
            address: "500 Chestnut St #1100, Abilene, TX 79602",
            number: "(325) 795-4301",
            speak_spanish: "No Spanish Resources",
            id: 1
        };
        render(<Router><ProgramCard {...program} /></Router>);

        const programNumber = screen.getByText('(325) 795-4301');
        expect(programNumber).toBeInTheDocument();

        const programSpanish = screen.getByText('No Spanish Resources');
        expect(programSpanish).toBeInTheDocument();

        const programName = screen.getByText('Workforce Solutions of West Central Texas');
        expect(programName).toBeInTheDocument();
    });
});

describe('LibraryCard', () => {
    it('test 21: library card render', () => {
        const library = {
            id: "3",
            imageSrc: "/assets/libraries/SGT.Fernando.jpg",
            mapSrc: "https://www.openstreetmap.org/export/embed.html?bbox=-98.11167597770692%2C26.184804031860573%2C-98.10963213443758%2C26.185668123160564&amp;layer=mapnik",
            address: "416 N Tower Rd, Alamo, TX 78516",
            name: "Sergeant Fernando de la Rosa Memorial Library",
            city: "Alamo",
            county: "Hidalgo",
            hours_open: "Monday - Thursday  8am - 6pm Friday 8am - 5pm Closed Saturday & Sunday",
            square_footage: "13698",
            audio_items: "1",
            video_items: "911",
            latitude: "",
            longitude: ""
        };

        render(<Router><LibraryCard  {...library} /></Router>);

        const libraryCounty = screen.getByText('Hidalgo');
        expect(libraryCounty).toBeInTheDocument();

        const libraryName = screen.getByText('Sergeant Fernando de la Rosa Memorial Library');
        expect(libraryName).toBeInTheDocument();
    });
});

describe('LibraryCard', () => {
    it('test 22: another library card render', () => {
        const library = {
            id: "3",
            imageSrc: "/assets/libraries/SGT.Fernando.jpg",
            mapSrc: "https://www.openstreetmap.org/export/embed.html?bbox=-98.11167597770692%2C26.184804031860573%2C-98.10963213443758%2C26.185668123160564&amp;layer=mapnik",
            address: "416 N Tower Rd, Alamo, TX 78516",
            name: "Sergeant Fernando de la Rosa Memorial Library",
            city: "Alamo",
            county: "Hidalgo",
            hours_open: "Monday - Thursday  8am - 6pm Friday 8am - 5pm Closed Saturday & Sunday",
            square_footage: "13698",
            audio_items: "1",
            video_items: "911",
            latitude: "",
            longitude: ""
        };

        render(<Router><LibraryCard  {...library} /></Router>);

        const libraryCity = screen.getByText('Alamo');
        expect(libraryCity).toBeInTheDocument();

        const libraryFoot = screen.getByText('13698');
        expect(libraryFoot).toBeInTheDocument();

        const libraryVideo = screen.getByText('911');
        expect(libraryVideo).toBeInTheDocument();
    });
});