from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

host = "cs373-database.cfyis2awalqz.us-east-2.rds.amazonaws.com"
user = "admin"
password = "literatetx"
db = "cs373Database"
port = 3306
db_url = f"mysql+pymysql://{user}:{password}@{host}:{port}/{db}"

app.config["SQLALCHEMY_DATABASE_URI"] = db_url

db = SQLAlchemy(app)

class county(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    fips_code = db.Column(db.Integer)
    population = db.Column(db.Integer)
    percent_literacy_lacking = db.Column(db.Float)
    literacy_lower_bound = db.Column(db.Float)
    literacy_upper_bound = db.Column(db.Float)

class library(db.Model):
    id = db.Column(db.Integer, primary_key=True) # uses id
    name = db.Column(db.String(50))
    county = db.Column(db.String(20)) # db.ForeignKey(county.name)
    address = db.Column(db.String(200))
    city = db.Column(db.String(50))
    square_footage = db.Column(db.Integer)
    num_audio_items = db.Column(db.Integer)
    num_video_items = db.Column(db.Integer)

class program(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    address = db.Column(db.String(200))
    phone_number = db.Column(db.String(20))
    speak_spanish = db.Column(db.Boolean)
    county = db.Column(db.String(20)) # db.ForeignKey(county.name)
