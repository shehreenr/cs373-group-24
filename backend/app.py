from flask import jsonify, request
from models import app, db, county, library, program
from schemas import CountySchema, LibrarySchema, ProgramSchema
from db import populate_db
from sqlalchemy import or_
from sqlalchemy.sql import desc


county_schema = CountySchema()
library_schema = LibrarySchema()
program_schema = ProgramSchema()

@app.route("/")
def home():
    return "<h1>This is the API for literatetx.me</h1> "

@app.route("/get_counties")
def get_counties():
    name = request.args.get("name")
    population = request.args.get("population")
    percent_literacy_lacking = request.args.get("percent_literacy_lacking")
    literacy_lower_bound = request.args.get("literacy_lower_bound")
    literacy_upper_bound = request.args.get("literacy_upper_bound")
    sort = request.args.get("sort")
    asc = request.args.get("asc")
    query = db.session.query(county)

    # filtering
    if name is not None:
        query = query.filter(county.name == name)
    if population is not None:
        range = population.split("-")
        try:
            query = query.filter(
                county.population >= range[0], county.population <= range[1]
            )
        except:
            pass
    if percent_literacy_lacking is not None:
        range = percent_literacy_lacking.split("-")
        try:
            query = query.filter(
                county.percent_literacy_lacking >= range[0], county.percent_literacy_lacking <= range[1]
            )
        except:
            pass
    if literacy_lower_bound is not None:
        range = literacy_lower_bound.split("-")
        try:
            query = query.filter(
                county.literacy_lower_bound >= range[0], county.literacy_lower_bound <= range[1]
            )
        except:
            pass
    if literacy_upper_bound is not None:
        range = literacy_upper_bound.split("-")
        try:
            query = query.filter(
                county.literacy_upper_bound >= range[0], county.literacy_upper_bound <= range[1]
            )
        except:
            pass

    # sorting
    if sort is not None and getattr(county, sort) is not None:
        if asc is not None:
            query = query.order_by(getattr(county, sort))
        else:
            query = query.order_by(desc(getattr(county, sort)))


    result = county_schema.dump(query, many=True)
    return jsonify({"result": result})

@app.route("/get_libraries")
def get_libraries():
    name = request.args.get("name")
    county = request.args.get("county")
    address = request.args.get("address")
    city = request.args.get("city")
    square_footage = request.args.get("square_footage")
    num_audio_items = request.args.get("num_audio_items")
    num_video_items = request.args.get("num_video_items")
    sort = request.args.get("sort")
    asc = request.args.get("asc")
    query = db.session.query(library)

    # filtering
    if name is not None:
        query = query.filter(library.name == name)
    if county is not None:
        query = query.filter(library.county == county)
    if address is not None:
        query = query.filter(library.address == address)
    if city is not None:
        query = query.filter(library.city == city)
    if square_footage is not None:
        range = square_footage.split("-")
        try:
            query = query.filter(
                library.square_footage >= range[0], library.square_footage <= range[1]
            )
        except:
            pass
    if num_audio_items is not None:
        range = num_audio_items.split("-")
        try:
            query = query.filter(
                library.num_audio_items >= range[0], library.num_audio_items <= range[1]
            )
        except:
            pass
    if num_video_items is not None:
        range = num_video_items.split("-")
        try:
            query = query.filter(
                library.num_video_items >= range[0], library.num_video_items <= range[1]
            )
        except:
            pass

    # sorting
    if sort is not None and getattr(library, sort) is not None:
        if asc is not None:
            query = query.order_by(getattr(library, sort))
        else:
            query = query.order_by(desc(getattr(library, sort)))
    


    result = library_schema.dump(query, many=True)
    return jsonify({"result": result})

@app.route("/get_programs")
def get_programs():
    name = request.args.get("name")
    address = request.args.get("address")
    phone_number = request.args.get("phone_number")
    speak_spanish = request.args.get("speak_spanish")
    county = request.args.get("county")
    sort = request.args.get("sort")
    asc = request.args.get("asc")

    query = db.session.query(program)

    # filtering
    if name is not None:
        query = query.filter(program.name == name)
    if address is not None:
        query = query.filter(program.address == address)
    if phone_number is not None:
        query = query.filter(program.phone_number == phone_number)
    if speak_spanish is not None:
        if speak_spanish == 'True':
            query = query.filter(program.speak_spanish == True)
        else:
            query = query.filter(program.speak_spanish == False)
    if county is not None:
        query = query.filter(program.county == county)
    
    # sorting
    if sort is not None and getattr(program, sort) is not None:
        if asc is not None:
            query = query.order_by(getattr(program, sort))
        else:
            query = query.order_by(desc(getattr(program, sort)))


    result = program_schema.dump(query, many=True)
    return jsonify({"result": result})

@app.route("/get_counties/<county_id>")
def get_county(county_id):
    query = db.session.query(county).filter(county.id == county_id)
    counties = []
    for county_instance in query:
        if county_instance.id is None:
            continue
        county_entry = {}
        county_entry["id"] = county_instance.id
        county_entry["name"] = county_instance.name
        county_entry["fips_code"] = county_instance.fips_code
        county_entry["population"] = county_instance.population
        county_entry["percent_literacy_lacking"] = county_instance.percent_literacy_lacking
        county_entry["literacy_lower_bound"] = county_instance.literacy_lower_bound
        county_entry["literacy_upper_bound"] = county_instance.literacy_upper_bound
        counties.append(county_entry)
    return jsonify(counties[0])

@app.route("/get_libraries/<library_id>")
def get_library(library_id):
    query = db.session.query(library).filter(library.id == library_id)
    libraries = []
    for library_instance in query:
        if library_instance.id is None:
            continue
        library_entry = {}
        library_entry["id"] = library_instance.id
        library_entry["name"] = library_instance.name
        library_entry["county"] = library_instance.county
        library_entry["address"] = library_instance.address
        library_entry["city"] = library_instance.city
        library_entry["square_footage"] = library_instance.square_footage
        library_entry["num_audio_items"] = library_instance.num_audio_items
        library_entry["num_video_items"] = library_instance.num_video_items
        libraries.append(library_entry)
    return jsonify(libraries[0])

@app.route("/get_programs/<program_id>")
def get_program(program_id):
    query = db.session.query(program).filter(program.id == program_id)
    programs = []
    for program_instance in query:
        if program_instance.id is None:
            continue
        program_entry = {}
        program_entry["id"] = program_instance.id
        program_entry["name"] = program_instance.name
        program_entry["county"] = program_instance.county
        program_entry["address"] = program_instance.address
        program_entry["phone_number"] = program_instance.phone_number
        program_entry["speak_spanish"] = program_instance.speak_spanish
        program_entry["county"] = program_instance.county
        programs.append(program_entry)
    return jsonify(programs[0])

@app.route("/search/<string:query>")
def search_all(query):
    county_result = search_counties(query).get_json()['data']
    program_result = search_programs(query).get_json()['data']
    library_result = search_libraries(query).get_json()['data']
    counties = county_schema.dump(county_result, many=True)
    programs = program_schema.dump(program_result, many=True)
    libraries = library_schema.dump(library_result, many=True)
    return jsonify(
        {"counties": counties, "programs": programs, "libraries": libraries}
    )

@app.route("/search/counties/<string:query>")
def search_counties(query):

    params = query.split()

    queries = []
    for param in params:
        try:
            queries.append(county.fips_code == int(param))
            queries.append(county.population == int(param))
        except:
            pass
        try:
            queries.append(county.name.icontains(param))
        except:
            pass
        try:
            queries.append(county.percent_literacy_lacking == float(param))
            queries.append(county.literacy_lower_bound == float(param))
            queries.append(county.literacy_upper_bound == float(param))
        except:
            pass
    entries = county.query.filter(or_(*queries))
    count = entries.count()
    result = county_schema.dump(entries, many=True)
    return jsonify({"data": result, "count": count})

@app.route("/search/programs/<string:query>")
def search_programs(query):
    params = query.split()

    queries = []
    for param in params:
        try:
            queries.append(program.name.icontains(param))
            queries.append(program.address.icontains(param))
            queries.append(program.phone_number.icontains(param))
            queries.append(program.county.icontains(param))
        except:
            pass
        try:
            if param == 'True':
                queries.append(program.speak_spanish == True)
            elif param == 'False':
                queries.append(program.speak_spanish == False)
        except:
            pass
    entries = program.query.filter(or_(*queries))
    count = entries.count()
    result = program_schema.dump(entries, many=True)
    return jsonify({"data": result, "count": count})

@app.route("/search/libraries/<string:query>")
def search_libraries(query):
    params = query.split()

    queries = []
    for param in params:
        try:
            queries.append(library.square_footage == int(param))
            queries.append(library.num_audio_items == int(param))
            queries.append(library.num_video_items == int(param))
        except:
            pass
        try:
            queries.append(library.name.icontains(param))
            queries.append(library.address.icontains(param))
            queries.append(library.city.icontains(param))
            queries.append(library.county.icontains(param))
        except:
            pass
    entries = library.query.filter(or_(*queries))
    count = entries.count()
    result = library_schema.dump(entries, many=True)
    return jsonify({"data": result, "count": count})


if __name__ == "__main__":
    with app.app_context():
        app.run(port=5000, debug=True)
