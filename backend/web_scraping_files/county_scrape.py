from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
import json

def scrape_county_website():

    PATH="C:\Program Files (x86)\chromedriver.exe"

    s = Service(PATH)
    browser_options = Options()
    browser_options.add_argument("--headless")
    browser_options.add_argument("--no-sandbox")
    driver = webdriver.Chrome(service=s, options = browser_options)

    driver.get("https://nces.ed.gov/naal/estimates/stateestimates.aspx")

    texas_option = driver.find_element(By.XPATH, "//option[45]")

    texas_option.click()

    all_counties_select = Select(driver.find_element(By.NAME, "ctl00$ContentPlaceHolder1$lbCounties"))
    all_counties_select.deselect_all()
    all_counties_option = all_counties_select.options[1]

    all_counties_option.click()

    go_button = driver.find_element(By.ID, "ctl00_ContentPlaceHolder1_btnGo")

    go_button.click()

    table = driver.find_element(By.ID, 'ctl00_ContentPlaceHolder1_tblEstimatesResults')

    result = {"results": []}

    rows = table.find_elements(By.TAG_NAME, 'tr')
    for row in rows[3:]:
        col = row.find_elements(By.TAG_NAME, 'td')
        county = col[0].text
        fips_code = col[1].text
        population_size = col[2].text
        percent_lacking = col[3].text
        lower_bound = col[4].text
        upper_bound = col[5].text
        if ',' in population_size:
            population_size = ''.join(population_size.split(','))
        entry = {
            'county_name': county,
            'fips_code': int(fips_code),
            'county_population': int(population_size),
            'percent_literacy_lacking': float(percent_lacking),
            'literacy_lower_bound': float(lower_bound),
            'literacy_upper_bound': float(upper_bound)
        }
        result["results"].append(entry)

    with open('backend/web_scraping_data/counties.json', 'w') as f:
        json.dump(result, f)

    driver.close()

if __name__ == '__main__':
    scrape_county_website()