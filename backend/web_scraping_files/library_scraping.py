import pandas as pd
import json


def scrape_libraries():
    df = pd.read_csv('backend/web_scraping_data/libraries.csv')
    
    result = {"results": []}

    for i in range(1, len(df) - 2):
        id = df.loc[i, 'ID']
        name = df.loc[i, '1.1 Library Name']
        county = df.loc[i, '1.2 County']
        address = df.loc[i, '1.5 Mailing Address']
        city = df.loc[i, '1.6 Mailing City']
        square_footage = df.loc[i, '2.4 Square Footage Main Library']
        num_audio_items = df.loc[i, '6.4 Physical Audio Items']
        num_video_items = df.loc[i, '6.5 Physical Video Items']

        if ',' in square_footage:
            square_footage = ''.join(square_footage.split(','))
        if ',' in num_audio_items:
            num_audio_items = ''.join(num_audio_items.split(','))
        if ',' in num_video_items:
            num_video_items = ''.join(num_video_items.split(',')) 

        entry = {
            "id": int(id),
            "library_name": name,
            "county": county,
            "address": address,
            "city": city,
            "square_footage": int(square_footage),
            "num_audio_items": int(num_audio_items),
            "num_video_items": int(num_video_items)
        }

        result["results"].append(entry)

    with open('backend/web_scraping_data/libraries.json', 'w') as f:
        json.dump(result, f)

if __name__ == '__main__':
    scrape_libraries()

