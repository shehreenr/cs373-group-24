from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import json
import time


def scrape_program_website():

    PATH="C:\Program Files (x86)\chromedriver.exe"

    s = Service(PATH)
    browser_options = Options()
    browser_options.add_argument("--headless")
    browser_options.add_argument("--no-sandbox")
    
    driver = webdriver.Chrome(service=s, options = browser_options)
    driver.get("https://www.literacytexas.org/connect/find-a-program/")
    time.sleep(5)


    result = {"results": []}
    rows = driver.find_elements(By.CLASS_NAME, "sl-item")
    for row in rows:
        row = row.text.split("\n")
        name = row[0]
        address = f"{row[1]} {row[2]}"
        phone_number = row[3]
        email = row[4]
        if 'None' in row[5]:
            counties_served = None
        elif ':' in row[5]:
            counties_served = row[5].split(": ")[1]
            if ',' in counties_served:
                counties_served = counties_served.split(',')[0]
        else:
            counties_served = None

        spanish = row[6] == 'Se habla español'

        entry = {
            'program_name': name,
            'address': address,
            'phone_number': phone_number,
            'email_address': email,
            'spanish': spanish,
            'county': counties_served
        }
        
        result["results"].append(entry)


    with open('backend/web_scraping_data/programs.json', 'w') as f:
        json.dump(result, f)

    driver.close()
    
if __name__ == "__main__":
    scrape_program_website()