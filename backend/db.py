import json, time
from models import county, library, program, db
from app import app

def populate_db():
    # populate_counties()
    # populate_libraries()
    # populate_programs()
    pass

def populate_counties():
    file = open("web_scraping_data/counties.json")
    county_data = json.load(file)
    file.close()
    num_added = 0
    for county_entry in county_data["results"]:
        db_row = {
            "id": num_added,
            "name": county_entry["county_name"],
            "fips_code": county_entry["fips_code"],
            "population": county_entry["county_population"],
            "percent_literacy_lacking": county_entry["percent_literacy_lacking"],
            "literacy_lower_bound": county_entry["literacy_lower_bound"],
            "literacy_upper_bound": county_entry["literacy_upper_bound"]
        }

        db.session.add(county(**db_row))
        time.sleep(1)
        num_added += 1
        print(num_added)
    db.session.commit()

def populate_libraries():
    file = open("web_scraping_data/libraries.json")
    library_data = json.load(file)
    file.close()
    num_added = 0
    for library_entry in library_data["results"]:
        db_row = {
            "id": library_entry["id"],
            "name": library_entry["library_name"],
            "county": library_entry["county"],
            "address": library_entry["address"],
            "city": library_entry["city"],
            "square_footage": library_entry["square_footage"],
            "num_audio_items": library_entry["num_audio_items"],
            "num_video_items": library_entry["num_video_items"]
        }

        db.session.add(library(**db_row))
        time.sleep(1)
        num_added += 1
        print(num_added)
    db.session.commit()

def populate_programs():
    file = open("web_scraping_data/programs.json")
    program_data = json.load(file)
    file.close()
    num_added = 0
    for program_entry in program_data["results"]:
        db_row = {
            "id": num_added,
            "name": program_entry["program_name"],
            "address": program_entry["address"],
            "phone_number": program_entry["phone_number"],
            "speak_spanish": program_entry["spanish"],
            "county": program_entry["county"]
        }

        db.session.add(program(**db_row))
        time.sleep(1)
        num_added += 1
        print(num_added)
    db.session.commit()