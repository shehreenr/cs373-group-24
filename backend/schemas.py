from models import county, library, program
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema 

class CountySchema(SQLAlchemyAutoSchema):
    class Meta:
        model = county

class LibrarySchema(SQLAlchemyAutoSchema):
    class Meta:
        include_fk = True
        model = library

class ProgramSchema(SQLAlchemyAutoSchema):
    class Meta:
        include_fk = True
        model = program