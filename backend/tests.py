import app
import unittest
import json

class Tests(unittest.TestCase):
    def setUp(self):
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()
        self.status_code = 200
    
    def testGetAllCounties(self):
        with self.client:
            response = self.client.get("/get_counties")
            self.assertEqual(self.status_code, response)
            data = response.json["result"]
            file = open("web_scraping_data/counties.json")
            county_data = json.load(file)
            file.close()
            self.assertEqual(len(data), len(county_data["result"]))

    def testGetAllLibraries(self):
        with self.client:
            response = self.client.get("/get_libraries")
            self.assertEqual(self.status_code, response)
            data = response.json["result"]
            file = open("web_scraping_data/libraries.json")
            library_data = json.load(file)
            file.close()
            self.assertEqual(len(data), len(library_data["result"]))

    def testGetAllPrograms(self):
        with self.client:
            response = self.client.get("/get_programs")
            self.assertEqual(self.status_code, response)
            data = response.json["result"]
            file = open("web_scraping_data/programs.json")
            program_data = json.load(file)
            file.close()
            self.assertEqual(len(data), len(program_data["result"]))

    def testGetSingleCounty(self):
        with self.client:
            response = self.client.get("/get_counties/2")
            self.assertEqual(self.status_code, response)
            data = response.json
            file = open("web_scraping_data/counties.json")
            county_data = json.load(file)
            file.close()
            self.assertEqual(data["id"], county_data["result"][3]["id"])

    def testGetSingleLibrary(self):
        with self.client:
            response = self.client.get("/get_libraries/2")
            self.assertEqual(self.status_code, response)
            data = response.json
            file = open("web_scraping_data/libraries.json")
            library_data = json.load(file)
            file.close()
            self.assertEqual(data["id"], library_data["result"][0]["id"])

    def testGetSingleProgram(self):
        with self.client:
            response = self.client.get("/get_programs/2")
            self.assertEqual(self.status_code, response)
            data = response.json
            file = open("web_scraping_data/programs.json")
            program_data = json.load(file)
            file.close()
            self.assertEqual(data["id"], program_data["result"][3]["id"])
    
    def testGetFilteredCounties(self):
        with self.client:
            response = self.client.get("/get_counties?population=30000-40000&sort=name&asc=")
            self.assertEqual(self.status_code, response)
            data = response.json['result']
            self.assertEqual(len(data), 17)

    def testGetFilteredLibraries(self):
        with self.client:
            response = self.client.get("/get_libraries?county=Dallas&square_footage=20000-40000")
            self.assertEqual(self.status_code, response)
            data = response.json['result']
            self.assertEqual(len(data), 7)

    def testGetFilteredPrograms(self):
        with self.client:
            response = self.client.get("/get_programs?speak_spanish=True&county=Tarrant&sort=address")
            self.assertEqual(self.status_code, response)
            data = response.json['result']
            self.assertEqual(len(data), 23)

if __name__ == "__main__":
    unittest.main()
