PLEASE NOTE: API performance can be a bit slow. It will load but may need a minute or two to load instances.

**Group 24**

**Team Member Information:**

| Name                    | GitLab ID         | EID     | Estimated Work Time | Actual Work Time |
| :---------------------- | ----------------- | ------- | ------------------- | ---------------- |
| Shyamli N Channabasappa | @shyamc23         | snc2535     |         6           |      7          |
| Sam Gupta               | @sgg877           | sgg877  |         5          |     5           |
| Jerry Lin               | @jerry_lin        | jl82333 |         7           |     7           |
| Shehreen Rahman         | @shehreenr        | sr53259 |         5           |     6           |
| Ritvik Renikunta        | @ritvik.renikunta | RSR2535 |         8           |     8           |

**Git SHA**: 7d19f234e2b1999fcd8290f4c6920f5be4610b92

**Pipeline**: https://gitlab.com/shehreenr/cs373-group-24/-/pipelines/1265335466

**Phase 4 Leader**: Sam Gupta

**Responsibilites**: Oversee progress, schedule TA & general meetings, check-in with team members, assign tasks to members 

**Website link:** https://literatetx.me

**Backend API:** https://cs373-backend.literatetx.me/

**API documentation:** https://documenter.getpostman.com/view/32906967/2sA2r6WPJ8

**Technical Report:** https://docs.google.com/document/d/1Kqb4t-UewTMUQhaWTEiD8AO5XdyWFLsZ6r_zvqp_UxU/edit?usp=sharing

**Pecha Kucha:** https://youtu.be/lPyRoR9QBS4 

**Underserved Community:** Adults with low-literacy in TX

**Project Name:** LiterateTX

**Description:** 
    As of 2023, Texas ranked the third lowest in adult literacy rate, with a rate of 71.8%. Illiteracy leads to less employment opportunities, low income, and high rates of crime. It is also a generational issue, as parents with low-literacy often do not raise literate children. Our mission with LiterateTX is to raise awareness for Texan adults who are illiterate and highlight the barriers they may face. We will also provide resources for low-literate adults who may want to improve their literacy skills.  

**Model 1: Counties**  
Attributes: Population size, number of people lacking literacy, county, percent of men lacking literacy, percent of women lacking literacy 
https://nces.ed.gov/naal/estimates/stateestimates.aspx
https://data.amerigeoss.org/de/dataset/piaac-county-indicators-of-adult-literacy-and-numeracy/resource/5fc42389-5fc2-4b8e-9916-1d9d4d3eb86d?inner_span=True  
Instances: 254
Connects to adult literacy program model and texas public library model  
Types of media: image, text
REST API for images and some text: https://www.mediawiki.org/wiki/API:Main_page

**Model 2: Literacy Programs**  
Attributes:Name, county, address, phone number, spanish or not (filter)  
https://www.literacytexas.org/connect/find-a-program/  
Instances: 314  
Connects to county literacy rates model and public library model  
Types of media: text, map, image(non-unique)

**Model 3: Texas Public Libraries**  
Attributes: library name, county, number of hours open, square footage of library, number of audio items, number of video items (resources for such people)
https://www.tsl.texas.gov/ldn/statistics  
Instances: 519  
Connects to county literacy rate model and adult literacy program model  
Types of media: map, text, image(non-unique)
REST API for Maps (displaying county or building locations on map): https://developers.google.com/maps/documentation/embed/quickstart#enable-api-sdk

**Questions our project will answer:**  
What resources are available for Texan adults who want to improve their literacy?  
What is the literacy rate of each Texan county and how do they compare with one another?  
Where are public libraries located and how accessible are they for low-literacy adults? 
